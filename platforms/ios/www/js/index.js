/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

app.medias = {} ;
app.periodTimeGpsAuto = 5 * 1000 ;  // toutes les minutes
app.char2sound = {

  "0" : "digits/zero",
  "1" : "digits/unite",
  "2" : "digits/deux",
  "3" : "digits/trois",
  "4" : "digits/quatre",
  "5" : "digits/cinq",
  "6" : "digits/six",
  "7" : "digits/sept",
  "8" : "digits/huit",
  "9" : "digits/neuf",

  "A" : "letters/alpha",
  "B" : "letters/bravo",
  "C" : "letters/charlie",
  "D" : "letters/delta",
  "E" : "letters/echo",
  "F" : "letters/foxtrot",
  "G" : "letters/golf",
  "H" : "letters/hotel",
  "I" : "letters/india",
  "J" : "letters/juliette",
  "K" : "letters/kilo",
  "L" : "letters/lima",
  "M" : "letters/mike",
  "N" : "letters/november",
  "O" : "letters/oscar",
  "P" : "letters/papa",
  "Q" : "letters/quebec",
  "R" : "letters/romeo",
  "S" : "letters/sierra",
  "T" : "letters/tango",
  "U" : "letters/uniform",
  "V" : "letters/victor",
  "W" : "letters/whisky",
  "X" : "letters/xray",
  "Y" : "letters/yankee",
  "Z" : "letters/zoulou"

}


app.messages = [
  {
    id : "sos",
    groupe : "vol",
    title : "sos",
    fulltitle : "SOS",
    message : "L'appareil immatricule {Aircraft} est en detresse"
  },
  {
    id : "report-position",
    groupe : "vol",
    title : "reportpos",
    fulltitle : "report de position",
    message : "L'appareil immatricule {Aircraft} est a la position {Position}"
  },
  {
    id : "pending-take-off",
    groupe : "sol",
    title : "attente decollage",
    fulltitle : "Attente decollage",
    message : "L'appareil immatricule {Aircraft} est en attente de decollage sur la piste {c:piste}"
  },
  {
    id : "alignement",
    groupe : "sol",
    title : "allignement piste",
    fulltitle : "Alignement sur la piste",
    message : "L'appareil immatricule {Aircraft} est a la position {Position}"
  }


];

document.addEventListener("deviceready", function() {
  // chargement des fichiers de lettres et chiffres
  var i ;
  for(var identifier in app.char2sound)
  {
    app.medias.push(new MediaController("sounds/" + app.char2sound[identifier] + ".mp3"))
  }
}, false);

$(function() {

  app.aircraft = localStorage.getItem("aircraft-current") ;

  if(app.aircraft === null || app.aircraft === undefined)
  {
    alert("Avant de pouvoir utiliser l'application, vous devez selectionner un appareil");
    document.location.href = "aircraft.html";
  }
  else
  {
    alert("Vous utilisez l'appareil immatricule " + app.aircraft );
  }



  $.each(app.messages, function(i, msg){

    //si le groupe existe deja
    if($('.groupe .'+msg.groupe).length === 0){
      $('#list-button').append($('<div>').addClass('groupe ' + msg.groupe).append(
        $('<h1>' + msg.groupe + '</h1>')
      )) ;
    }
    $('.groupe.'+msg.groupe).append($('<button>').html(msg.fulltitle).prop('data-msg', i)
      .on('click', function() {

          app.sendMessageThroughRadio($(this).prop('data-msg'));
      }))


  })


  $('.gps-report-auto').on('click',  app.toggleGPSauto);
  $('.sos').on('click', function() {
    alert('SOS envoyé');
    app.sendMessageThroughRadio("sos");


  });
  $('.your-profile').on('click', function() {
    document.location.href = "profile.html";
  })






})


app.sendMessageThroughRadio = function(idmessage) {

  var message = app.messages.find(function(v) { return v.id === idmessage});
  message.message.trim();

  // analyse du texte : on avance caractere par caractere
  // des qu'on trouve un { on regarde ce qu'il y a dds, mode : param
  var mode = "text" ,
  currentParam = "" ,
  i = 0,
  messagePart = 0,
  alreadyPlayedSoundC = false,
  speech = [];

  while(i < message.message.length)
  {
    var c = message.message[i];
    if(mode === "param") {
      if(c === "}")
      {
        // end of param
        speech = speech.concat(app.analyseParam(currentParam));
        currentParam = "" ;
        alreadyPlayedSoundC = false ;
        mode = "text" ;
      }

      else
        currentParam += c;

    }
    else if(c === "{")
      mode = "param";

    else if(!alreadyPlayedSoundC) {
      speech.push(message.id + "_" + messagePart );
      messagePart++ ;
      alreadyPlayedSoundC = true;

    }

    i++ ;
  }


  app.speechToRadio(speech);





}
app.analyseParam = function(param)
{
  var toAdd = [];

  if(param === "Aircraft")
  {
    var i ;
    for(i = 0; i < app.aircraft.length; i++)
    {
      toAdd.push(app.char2sound[app.aircraft[i]]);
    }
  }

  return toAdd;
}

app._loadSpeech = function(speech) {
  app.numberfileloaded = 0;
  return new Promise(function(resolve, reject){

    for(i = 0; i < speech.length; i++) {

      if (app.medias[speech[i]] == null){
          app.medias[speech[i]] = new Howl({src: "sounds/" + speech[i] + ".mp3"})
        app.medias[speech[i]].once("load", function () {

          app.numberfileloaded++;
          if (app.numberfileloaded === speech.length - 1)
            resolve(app.medias); // s'execute quand tout les fichiers du speech sont chargés

          /*
        }
        , function(){
          console.error("unable to load " + speech[i] + ".mp3");
          reject();
          */
        });
      }
      else {
        // il est deja chargé
        app.numberfileloaded++;
        if (app.numberfileloaded === speech.length - 1)
          resolve(app.media);
      }


    }

  });

}
app.speechToRadio = function(speech) {

  // on charge les medias
  app._loadSpeech(speech).then(function(m){
    // on a mnt tous les fichiers dans app.media
    // on va les jouer les uns a la suite des autres dans l'ordre du speech
    var timePassed = 0
    var i = 0;

    for(i = 0; i < speech.length; i++)
    {
      (function(i, timePassed) {
        setTimeout(function () {
          app.medias[speech[i]].play();
        }, timePassed);
      })(i, timePassed * 1000);

      timePassed += app.medias[speech[i]]._duration; // en s !
    }

  })
}

app.toggleGPSauto = function() {
  var $statusGpsStatus = $('.gps-auto-status');

  if($statusGpsStatus.html() === "OFF")
  {
    // on l'active
    $statusGpsStatus.html("ON").parent('.gps-report-auto').css('background', '#0c0');
    app.gpsAutoPeriod = window.setInterval(function() {
      app.sendMessageThroughRadio("report-position")
    }, app.periodTimeGpsAuto)


  }
  else {
    $statusGpsStatus.html("OFF").parent('.gps-report-auto').css('background', 'grey');
    clearInterval(app.gpsAutoPeriod);
  }

}
app.initialize();